definition module System._WinDef

:: HANDLE	:== Int
:: DWORD	:== Int
:: LPDWORD	:== {#Int}
:: LPCTSTR	:== Int
:: LPTSTR	:== Int
:: LPVOID	:== Int
:: LPCVOID	:== Int
:: SIZE_T	:== Int

:: HLOCAL   :== HANDLE

INVALID_HANDLE_VALUE :== -1

NULL :== 0

MAX_PATH :== 260
