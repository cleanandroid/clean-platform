definition module Data.Void
/**
* This module defines the empty type 'Void' which means something like 'no value'.
*/
:: Void	= Void
